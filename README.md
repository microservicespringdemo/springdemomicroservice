# Récupérer les submodules

`git submodule update --init --recursive`

ne pas modifier les projets directement depuis ce repo, sinon tout va casser.

Mettre à jours les projets depuis leurs repos respectifs et faire la mise à jour du projet principal avec la commande suivante : 

`git submodule update --remote`